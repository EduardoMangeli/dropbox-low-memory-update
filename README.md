# Dropbox low memory update

A method to update dropbox in low memory machines

## Description
This method simply put a line on root crontab to execute a script that verifies
if dropbox is running (and updated).

 - put the `dropbox_cron.sh` in your user home dir
 - update the `/etc/crontab` to include the following line changing _mangeli_ by your user
```
0,15,30,45 * * * * mangeli flock -xn /home/mangeli/lock.lck -c /home/mangeli/dropbox_cron.sh
```