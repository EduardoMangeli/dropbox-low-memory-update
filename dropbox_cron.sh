#!/bin/bash

LOG_FILE="$HOME/logs/dropbox_cron"

writeLog(){
    local logdate=$(date "+%Y-%m-%d %H:%M:%S")
    local ACTUAL_LOG_FILE=$LOG_FILE-`date +%Y-%m-%d`.log
    if [[ -z $1 ]]; then
        echo "$logdate: Attempt to log an empty string." >> $ACTUAL_LOG_FILE 2>&1
    else
        echo "$logdate: $1" >> $ACTUAL_LOG_FILE 2>&1
    fi
}

isDropboxRunning(){
    writeLog "Testing if dropbox is running."
    local text=$(dropbox status)
    if [[ $text == "Dropbox isn't running!" ]]; then
        isRunning=0
    else
        isRunning=1
    fi
}

isDropboxUpDated(){
    writeLog "Testing if dropbox is up to date."
    local text=$(dropbox status)
    if [[ $text == "Atualizado" || $text == "Up to date" ]]; then
        isUpDated=1
    else
        isUpDated=0
    fi

}

writeLog "Starting ${0}."


isDropboxRunning

if [ $isRunning -eq 0 ]; then
    writeLog "Dropbox isn't running."
    writeLog "Starting Dropbox."
    dropbox start
fi
writeLog "Dropbox is running."
isDropboxUpDated
while [ $isUpDated -eq 0 ]; do
    sleep 30s
    isDropboxUpDated
done
writeLog "Dropbox is up to date."
writeLog "Stopping Dropbox."
dropbox stop
writeLog "Stopping ${0}."
